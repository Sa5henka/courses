def lets_check_who_is_winner(lst):
    '''collects data from  the lists  and other'''
    spisok=lst[:]    #create list
    stolbec=list(zip(*lst))    #check list items
    numbering1=[row[i] for i, row in enumerate(spisok)]    #numbering elements of list
    numbering2=[row[2 - i] for i, row in enumerate(spisok)]
    triplets=spisok+stolbec+[numbering1, numbering2]    #combination for winner

    for triplet in triplets:
        if len(set(triplet))==1 and triplet[0] != '.':
            return triplet[0]
    return 'D or Ничья'

print(lets_check_who_is_winner(["oox",
                                "xox",
                                "oxo"]))
