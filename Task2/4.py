one_to_nineteen = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight',
                   'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen',
                   'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen', '']    #dictionary 1
twenty_ninety = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy',
                 'eighty', 'ninety']    #dictionary 2


def stefan_speek(numbers):
    """try to find input number"""
    if not numbers:
        return 'Zero'
    if numbers // 100 and numbers % 100:    #numbers>100 second part of number
        result = '{} hundred '.format(one_to_nineteen[numbers // 100 - 1])
    elif numbers // 100:    #numers>100 first part of number
        result = '{} hundred'.format(one_to_nineteen[numbers // 100 - 1])
    else:
        result = ''
    if numbers % 100 <= 19:    #numbers<20
        result += one_to_nineteen[numbers % 100 - 1]
    else:    #20<numbers<100
        result = result + twenty_ninety[numbers % 100 // 10 - 2] + ' ' + one_to_nineteen[numbers % 10 - 1]
    return result


print('Please,input some numbers:')
numbers = int(input())
print(stefan_speek(numbers))
