def list_merge(lstlst):
    lst = []
    for i in lstlst:
        if isinstance(i, list):
            lst.extend(list_merge(i))
        else:
            lst.append(i)
    return lst
lstlst=[[1,2,[3,4]],[[5,6],7]]
print(list_merge(lstlst))